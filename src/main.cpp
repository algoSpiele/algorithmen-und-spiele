/**
 * @file main-example-client-dom.cpp
 * @author Martin Wistauder
 * @date 12 Oct 2020
 * @version 1.0
 */
#include <string>
#include <iostream>
#include <netcode.grpc.pb.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "GameClient.h"
#include "Helper.h"
#include "UserToken.h"
#include <cstdlib>
#include <ctime>

void autoPlay(example::dom::GameClient &client);

example::dom::StubPtr stub;
uint32_t boardWidth = 10;
uint32_t boardHeight = 10;
bool firstMove = true;

std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> ourMoves;

bool serverConnection()
{
    std::string host = "gameserver.ist.tugraz.at:80";
    INFO("Trying to connect to " << host);
    auto creds = grpc::InsecureChannelCredentials();
    auto channel = grpc::CreateChannel(host, creds);

    if (!channel->WaitForConnected(gpr_time_add(
            gpr_now(GPR_CLOCK_REALTIME),
            gpr_time_from_seconds(10, GPR_TIMESPAN))))
    {
        std::cout << "Error: Connection Timeout.\n";
        return false;
    }

    stub = netcode::GameCom::NewStub(channel);
    return true;
}

std::string getUserToken(UserToken *userToken)
{
    netcode::AuthPacket authPacket;
    authPacket.set_matr_number(userToken->matrNr);
    authPacket.set_secret(userToken->secret);

    netcode::GetUserTokenResponse response;
    grpc::ClientContext context;

    grpc::Status status = stub->GetUserToken(&context, authPacket, &response);

    if (!status.ok())
    {
        std::cout << "Error: The request has been canceled by the server.\n";
        return nullptr;
    }

    return response.user_token();
}

void requestNewMatch(example::dom::GameClient &client)
{
    INFO("Requesting new match.");
    client.newMatch(boardWidth, boardHeight);

    std::cout << "Waiting for opponent" << std::flush;
    while (!client.hasMatchStarted())
    {
        gpr_sleep_until(gpr_time_add(
            gpr_now(GPR_CLOCK_REALTIME),
            gpr_time_from_seconds(2, GPR_TIMESPAN)));
        std::cout << "." << std::flush;
    }
    std::cout << "\n";
    INFO("Opponent found.");
}

int main()
{
    auto *userToken = new UserToken();
    /* Check server connection */
    if (!serverConnection())
        return -1;

    /* Create clint with user token */
    example::dom::GameClient client(getUserToken(userToken));

    /* Find new match */
    requestNewMatch(client);

    /* Play */
    client.queryOpponentInfo();
    client.queryTimeout();

    uint32_t width = client._currentState.board_width();
    uint32_t height = client._currentState.board_height();

    while(!client.hasMatchStarted());
    autoPlay(client);

    return 0;
}

bool isMoveValid(example::dom::GameClient &client, int x_, int y_)
{
    uint32_t width = client._currentState.board_width();
    uint32_t height = client._currentState.board_height();
    const char *data = client._currentState.board_data().c_str();

    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            if (x == x_ && y == y_ && data[x + y * width] == '0')
                return true;
        }
    }

    return false;
}

void submitTurn(example::dom::GameClient &client)
{
    int x1, y1, x2, y2;
    int counter = 0;
    while (true)
    {
        if (counter % 500 == 0)
            client._beginningPlayer = !client._beginningPlayer;

        if (client._beginningPlayer)
        {
            x1 = (int)rand() % client._currentState.board_width();
            y1 = (int)rand() % client._currentState.board_height();
            x2 = x1 + 1;
            y2 = y1;
        }
        else if (!client._beginningPlayer)
        {
            x1 = (int)rand() % client._currentState.board_width();
            y1 = (int)rand() % client._currentState.board_height();
            x2 = x1;
            y2 = y1 + 1;
        }

        if (isMoveValid(client, x1, y1) && isMoveValid(client, x2, y2))
            break;

        counter++;
    }

    ::dom::GameTurn *turn = new ::dom::GameTurn();
    turn->set_x1(x1);
    turn->set_y1(y1);
    turn->set_x2(x2);
    turn->set_y2(y2);
    client.submitTurn(turn);
}

std::pair<int, int> findTheirMove(example::dom::GameClient &client)
{
    uint32_t width = client._currentState.board_width();
    uint32_t height = client._currentState.board_height();
    const char *data = client._currentState.board_data().c_str();

    for (int y = 0; y < height; ++y)
    {
        for (int x = 0; x < width; ++x)
        {
            if (data[x + y * width] == '1' && data[x + (y + 1) * width] == '1')
                return {y, y + 1};
            else if (data[x + y * width] == '1' && data[(x + 1) + y * width] == '1')
                return {y, y};
        }
    }

    return {width + 1, width + 1};
}
std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> findPath(example::dom::GameClient &client, std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> previousMoves);

void genMove(example::dom::GameClient &client)
{
    uint32_t width = client._currentState.board_width();
    uint32_t height = client._currentState.board_height();
    const char *data = client._currentState.board_data().c_str();

    int x1, y1, x2, y2;

    if (firstMove)
    {
        firstMove = false;
        if (client._beginningPlayer)
        {
            while (1)
            {
                int random = rand() % height;
                x1 = 0;
                y1 = random;
                x2 = 1;
                y2 = random;
                if (isMoveValid(client, x1, y1) && isMoveValid(client, x2, y2))
                    break;
            }
        }
        else if (!client._beginningPlayer)
        {
            while (1)
            {
                int random = rand() % height;
                x1 = random;
                y1 = 0;
                x2 = random;
                y2 = 1;
                if (isMoveValid(client, x1, y1) && isMoveValid(client, x2, y2))
                    break;
            }
        }
    }
    else
    {
        //std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> previousMoves = ourMoves;

        /* std::cout << "##############################" << std::endl;
        std::cout << "last x1 " << lastMove.first.first << " last y1 " << lastMove.first.second << " last x2 " << lastMove.second.first << " last y2 " << lastMove.second.second << std::endl;
        std::cout << "##############################" << std::endl; */
        x1 = -1;
        y1 = -1;
        x2 = -1;
        y2 = -1;
        while (!isMoveValid(client, x1, y1) && !isMoveValid(client, x2, y2))
        {
            std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> move = findPath(client, ourMoves);
            x1 = move[0].first.first;
            y1 = move[0].first.second;
            x2 = move[0].second.first;
            y2 = move[0].second.second;
        }
    }

    ourMoves.push_back({{x1, y1}, {x2, y2}});
    ::dom::GameTurn *turn = new ::dom::GameTurn();
    turn->set_x1(x1);
    turn->set_y1(y1);
    turn->set_x2(x2);
    turn->set_y2(y2);
    client.submitTurn(turn);
    client.showGameState(x1, y1, x2, y2);
}

std::pair<std::pair<int, int>, std::pair<int, int>> makeRedMove(int x, int y, uint32_t width, uint32_t height, const char *data)
{
    /* red player  */
    if (x + 1 < width && x + 2 < width && data[x + 1 + y * width] == '0' && data[x + 2 + y * width] == '0')
    {
        return {{x + 1, y}, {x + 2, y}};
    }
    else if (x + 1 < width && x + 2 < width && y + 1 < height && data[x + 1 + (y + 1) * width] == '0' && data[x + 2 + (y + 1) * width] == '0')
    {
        return {{x + 1, y + 1}, {x + 2, y + 1}};
    }
    else if (x + 1 < width && x + 2 < width && y - 1 >= 0 && data[x + 1 + (y - 1) * width] == '0' && data[x + 2 + (y - 1) * width] == '0')
    {
        return {{x + 1, y - 1}, {x + 2, y - 1}};
    }
    else if (x + 1 < width && y + 1 < height && data[x + (y + 1) * width] == '0' && data[x + 1 + (y + 1) * width] == '0')
    {
        return {{x, y + 1}, {x + 1, y + 1}};
    }
    else if (x + 1 < width && y - 1 >= 0 && data[x + (y - 1) * width] == '0' && data[x + 1 + (y - 1) * width] == '0')
    {
        return {{x, y - 1}, {x + 1, y - 1}};
    }
    else if (x - 2 >= 0 && x - 3 >= 0 && data[x - 2 + y * width] == '0' && data[x - 3 + y * width] == '0')
    {
        return {{x - 2, y}, {x - 3, y}};
    }
    else if (x - 2 >= 0 && x - 3 >= 0 && y - 1 >= 0 && data[x - 2 + (y - 1) * width] == '0' && data[x - 3 + (y - 1) * width] == '0')
    {
        return {{x - 2, y - 1}, {x - 3, y - 1}};
    }
    else if (x - 2 >= 0 && x - 3 >= 0 && y + 1 < height && data[x - 2 + (y + 1) * width] == '0' && data[x - 3 + (y + 1) * width] == '0')
    {
        return {{x - 2, y + 1}, {x - 3, y + 1}};
    }
    else if (x - 1 >= 0 && x - 2 >= 0 && y - 1 >= 0 && data[x - 1 + (y - 1) * width] == '0' && data[x - 2 + (y - 1) * width] == '0')
    {
        return {{x - 2, y - 1}, {x - 3, y - 1}};
    }
    else if (x - 1 >= 0 && x - 2 >= 0 && y + 1 < height && data[x - 1 + (y + 1) * width] == '0' && data[x - 2 + (y + 1) * width] == '0')
    {
        return {{x - 2, y + 1}, {x - 3, y + 1}};
    }
    else if (y - 1 >= 0 && x - 1 >= 0 && data[x - 1 + (y - 1) * width] == '0' && data[x + (y - 1) * width] == '0')
    {
        return {{x - 1, y - 1}, {x, y - 1}};
    }
    else if (y + 1 < height && x - 1 >= 0 && data[x - 1 + (y + 1) * width] == '0' && data[x + (y + 1) * width] == '0')
    {
        return {{x - 1, y + 1}, {x, y + 1}};
    }

    //std::cout << "helo " << " x1 " << i << " y1 " << j << std::endl;
}

std::pair<std::pair<int, int>, std::pair<int, int>> makeBlueMove(int x, int y, uint32_t width, uint32_t height, const char *data)
{
    /* blue player */
    if (y + 1 < height && y + 2 < height && data[x + (y + 1) * width] == '0' && data[x + (y + 2) * width] == '0')
    {
        return {{x, y + 1}, {x, y + 2}};
    }
    else if (y + 1 < height && y + 2 < height && x + 1 < width && data[x + 1 + (y + 1) * width] == '0' && data[x + 1 + (y + 2) * width] == '0')
    {
        return {{x + 1, y + 1}, {x + 1, y + 2}};
    }
    else if (y + 1 < height && y + 2 < height && x - 1 >= 0 && data[x - 1 + (y + 1) * width] == '0' && data[x - 1 + (y + 2) * width] == '0')
    {
        return {{x - 1, y + 1}, {x - 1, y + 2}};
    }
    else if (y + 1 < height && x + 1 < width && data[x + 1 + y * width] == '0' && data[x + 1 + (y + 1) * width] == '0')
    {
        return {{x + 1, y}, {x + 1, y + 1}};
    }
    else if (y + 1 < height && x - 1 >= 0 && data[x - 1 + y * width] == '0' && data[x - 1 + (y + 1) * width] == '0')
    {
        return {{x - 1, y}, {x - 1, y + 1}};
    }
    else if (y - 2 >= 0 && y - 3 >= 0 && data[x + (y - 2) * width] == '0' && data[x + (y - 3) * width] == '0')
    {
        return {{x, y - 2}, {x, y - 3}};
    }
    else if (y - 2 >= 0 && y - 3 >= 0 && x - 1 >= 0 && data[x - 1 + (y - 2) * width] == '0' && data[x - 1 + (y - 3) * width] == '0')
    {
        return {{x - 1, y - 2}, {x - 1, y - 3}};
    }
    else if (y - 2 >= 0 && y - 3 >= 0 && x + 1 >= 0 && data[x + 1 + (y - 2) * width] == '0' && data[x + 1 + (y - 3) * width] == '0')
    {
        return {{x + 1, y - 2}, {x + 1, y - 3}};
    }
    else if (y - 1 >= 0 && y - 2 >= 0 && x + 1 < width && data[x + 1 + (y - 1) * width] == '0' && data[x + 1 + (y - 2) * width] == '0')
    {
        return {{x + 1, y - 1}, {x + 1, y - 2}};
    }
    else if (y - 1 >= 0 && y - 2 >= 0 && x - 1 >= 0 && data[x - 1 + (y - 1) * width] == '0' && data[x - 1 + (y - 2) * width] == '0')
    {
        return {{x - 1, y - 1}, {x - 1, y - 2}};
    }
    else if (y - 1 >= 0 && x - 1 >= 0 && data[x - 1 + (y - 1) * width] == '0' && data[x - 1 + y * width] == '0')
    {
        return {{x - 1, y - 1}, {x - 1, y}};
    }
    else if (y - 1 >= 0 && x + 1 < width && data[x + 1 + (y - 1) * width] == '0' && data[x + 1 + y * width] == '0')
    {
        return {{x + 1, y - 1}, {x + 1, y}};
    }
}
std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> findPath(example::dom::GameClient &client, std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> previousMoves)
{
    uint32_t width = client._currentState.board_width();
    uint32_t height = client._currentState.board_height();
    const char *data = client._currentState.board_data().c_str();
    std::pair<std::pair<int, int>, std::pair<int, int>> lastMove = previousMoves.back();
    int x1 = lastMove.first.first, y1 = lastMove.first.second, x2 = lastMove.second.first, y2 = lastMove.second.second;
    std::vector<std::pair<std::pair<int, int>, std::pair<int, int>>> path;
    bool playerNr = client._beginningPlayer;

    for (size_t i = 0; i < 2; i++)
    {
        if (playerNr)
        {
            std::pair<std::pair<int, int>, std::pair<int, int>> combs = makeRedMove(x2, y2, width, height, data);
            path.push_back(combs);
        }
        else if (!playerNr)
        {
            std::pair<std::pair<int, int>, std::pair<int, int>> combs = makeBlueMove(x2, y2, width, height, data);
            path.push_back(combs);
        }
        x1 = path[0].first.first;
        y1 = path[0].first.second;
        x2 = path[0].second.first;
        y2 = path[0].second.second;

        if (!isMoveValid(client, x1, y1) && !isMoveValid(client, x2, y2) && i == 0)
        {
            path.clear();
            playerNr = !playerNr;
        }
        else if (!isMoveValid(client, x1, y1) && !isMoveValid(client, x2, y2) && i == 1)
        {
            std::cout<<"Else if";
            previousMoves.pop_back();
            path.clear();
            path = findPath(client, previousMoves);
        }
    }

    return path;
}

void autoPlay(example::dom::GameClient &client)
{
    while (!client.isMatchOver())
    {
        //std::cout<<client.isTurnPlayable()<<std::endl;
        if (!client.isMatchOver() && client.isTurnPlayable())
        {
            genMove(client);
            for (size_t i = 0; i < 20000000; i++)
            {
            }
        }
        client.queryGameState();
    }
}

/* 
        red player:
        x + 1, x + 2 done
        x + 1, x + 2, j + 1 done
        x + 1, x + 2, j - 1 done
        x, x + 1, j + 1 done
        x, x + 1, j - 1 done

        x - 2, x - 3 done 
        x - 2, x - 3, j - 1 done
        x - 2, x - 3, j + 1 done
        
        x - 1, x - 2, j - 1 done
        x - 1, x - 2, j + 1 done

        x - 1, x, y - 1 done
        x-1, x, y + 1 done
    */

/* 
        blue player:
        y + 1, y + 2 done
        y + 1, y + 2, x + 1 done
        y + 1, y + 2, x - 1 done
        y, y + 1, x + 1 done
        y, y + 1, x - 1 done

        y - 2, y - 3  done
        y - 2, y - 3, x - 1 done
        y - 2, y - 3, x + 1 done
        
        y - 1, y - 2, x - 1 done
        y - 1, y - 2, x + 1 done

        y - 1, y, x - 1 done
        y-1, y, x + 1 done
    */